# httprelay-js

JavaScript and [TypeScript](https://www.typescriptlang.org/) library for [HTTP Relay](https://httprelay.io/) server.
Currently, only `proxy` communication method is implemented.


## Installation 
[Source code respository](https://gitlab.com/jonas.jasas/httprelay-js).

To use as a JavaScript module:
```shell script
npm i httprelay
```

If you are planning to use this library from the local file system (without using webserver) please use the non-module version.
JavaScript modules are not loaded due to CORS restrictions.
To use as a classic JavaScript library include this in to your HTML:
```html
<script src="https://unpkg.com/httprelay/lib/non-mod/httprelay.js"></script>
```
it will load most recent library version. Better idea is to reference fixed version:
```html
<script src="https://unpkg.com/httprelay@0.0.44/lib/non-mod/httprelay.js"></script>
```
replace `0.0.44` with the version that you are going to use.


## Proxy communication method
`Proxy` communication method lets you use your web browser as a server.
There is no need to have a real IP address, this even works behind the firewall, NAT or web proxy server.
You can serve dynamic web pages, write your own API endpoints, even serve files.
For `proxy` communication method this library acts as a small framework with these features:

- Handlers (actions) - functions that will be called on HTTP request
- Routes - registered paths that will invoke handler functions
- Assets - images, JS, CSS that will be served to the client
- Templates - minimalistic HTML templates

### Examples
- [Basic usage](https://codesandbox.io/s/8mv5nc)
- [Interactive page](https://codesandbox.io/s/dn7no9)
- [File sharing](https://codesandbox.io/s/gcfe0f)
- [REST API](https://codesandbox.io/s/4uzsdw)
- [Assets](https://codesandbox.io/s/m1z9cu)

## Documentation
[HTTP Relay JavaScript library documentation](https://js-doc.httprelay.io/)
