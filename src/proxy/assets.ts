interface IAssetSettings {
    /**
     * Unique asset name. Used for referencing an asset.
     */
    name?: string,

    /**
     * Create a route for this asset. {@link Route.name} will be the same as {@link name}
     */
    mount?: boolean,

    /**
     * If {@link mount} is enabled, set if asset should be cached.
     */
    cache?: boolean

    /**
     * If the asset is a text or HTML, should it be parsed and `{HR{ expression }HR}` blocks evaluated.
     */
    interpolate?: boolean,
}

//*# nonModule !
import Routes from './routes.js'
import IAsset from './asset.js'
import AssetFetch from './asset-fetch.js'
import AssetText from './asset-text.js'
import HandlerCtx from './handler-ctx.js'

export {IAssetSettings}
export default
//*/

class Assets {
    private readonly assets: IAsset[] = []

    /**
     * Class should not be instantiated, please use {@link HrProxy.assets} instead.
     */
    constructor(
        private readonly routes: Routes,
        private readonly baseUrl: URL,
        private readonly assetPathPrefix: string=''
    ) { }

    /**
     * Adds new asset
     * @param asset - asset to be added.
     */
    public add(asset: IAsset): void {
        this.assets.push(asset)
        if (asset.settings.mount) {
            this.routes.addGet(
                `${this.assetPathPrefix}/${asset.settings.name}${asset.settings.cache ? btoa(Math.random().toString()).substr(5, 8) : ''}`,
                asset.settings.name ?? 'Not set',
                ctx => ctx.respond({
                    body: asset.settings.interpolate ? this.interpolate(asset, ctx) : asset.blob(),
                    headers: new Headers({
                        'Cache-Control': asset.settings.cache ? 'public, max-age=31536000, immutable' : 'no-store'
                    }),
                    mergeHeaders: true
                })
            )
        }
    }

    /**
     * @internal
     */
    public interpolate(asset: IAsset, ctx: HandlerCtx): Promise<string | undefined> {
        return asset.text().then(t => {
            let rx = /\{HR\{(?<expr>[\s\S]+?)\}HR\}/gm

            // Collecting promise array (not replacing, avoiding matchAll ES2020)
            let exprPros: Promise<string>[] = []
            t?.replace(rx, (m, g) => {
                (function(expr: string) {
                    let v = ''
                    try {
                        v = eval(expr)
                    } catch (e) {
                        console.error(e)
                    }
                    exprPros.push(Promise.resolve(v))
                }).call(ctx, g ?? '')
                return m
            })

            // Actually replacing with the resolved values
            return Promise.all(exprPros).then(v => t?.replace(rx, () => v.shift() ?? ''))
        })
    }

    /**
     * Adds remote asset (image, js, css, etc.). Can be used with the data URI (https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs).
     * @param url - URL of the asset.
     * @param settings - Asset settings.
     */
    public addFetch(url: string | URL, settings: IAssetSettings): IAsset {
        url = url instanceof URL ? url : new URL(url, this.baseUrl)
        let asset = new AssetFetch(url, settings)
        this.add(asset)
        return asset
    }

    /**
     * Adds text as an asset. Can be provided a function that returns string. Text can be interpolated if set in a settings param.
     * @param text - Text that will be used as an asset.
     * @param settings - Asset settings.
     */
    public addText(text: string | (() => string), settings: IAssetSettings): IAsset {
        let asset = new AssetText(text, settings)
        this.add(asset)
        return asset
    }

    /**
     * Same as {@link addFetch} but the URL is red from `link` tag `href` attribute. Usefull for data URIs (https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs).
     * @param linkEl - `link` DOM element.
     * @param settings - Asset settings.
     */
    public addLink(linkEl: HTMLLinkElement, settings?: IAssetSettings): IAsset | undefined {
        let defaults = this.assetDefaults(linkEl, settings)
        return this.addFetch(linkEl.href, defaults)
    }

    /**
     * Adds HTML template (can be any format text data). Template can be interpolated if set in a settings param.
     * @param scriptEl - `script` DOM element.
     * @param settings - Asset settings.
     */
    public addTemplate(scriptEl: HTMLScriptElement, settings?: IAssetSettings): IAsset | undefined {
        let defaults = this.assetDefaults(scriptEl,settings)
        return this.addText(scriptEl.innerHTML, defaults)
    }

    private assetDefaults(el: HTMLElement, settings?: IAssetSettings): IAssetSettings {
        return <IAssetSettings>{
            name: settings?.name ?? el.id,
            mount: settings?.mount ?? el.hasAttribute('data-mount'),
            interpolate: settings?.interpolate ?? el.hasAttribute('data-interpolate'),
            cache: settings?.cache ?? el.hasAttribute('data-cache')
        }
    }

    /**
     * Gets asset by name
     * @param name - asset name.
     */
    public get(name: string): IAsset | undefined {
        return this.assets.find(a => a.settings.name === name)
    }

    /**
     * Loads assets from the DOM automatically using element `id` as the asset name. DOM elements should be `httprelay-asset` class and can be configured using these `data-` attributes:
     *
     * - **data-mount** - creates a route for the asset.
     * - **data-cache** - caches asset by adding `cache-control` header field.
     * - **data-interpolate** - interpolates asset contents by evaluating JS inside `{HR{ expression }HR}`.
     */
    public autoLoad(): void {
        Array.from(document?.getElementsByClassName('httprelay-asset')).forEach(el => {
            if (el instanceof HTMLLinkElement) this.addLink(el)
            if (el instanceof HTMLScriptElement) this.addTemplate(el)
        })
    }
}