interface SelectedRoute {
    handlerFunc: HandlerFunc,
    params: RouteParams
}

//*# nonModule !
import Route, {HandlerFunc} from './route.js'
import {RouteParams} from './handler-ctx.js'

export default
//*/

/**
 * Routes class is responsible for route management.
 */
class Routes {
    private readonly routes: Route[] = []

    /**
     * Function is executed when the client is requesting unregistered route.
     * Handler should return custom `404 Not Found` page/message.
     */
    public notFoundHandlerFunc: HandlerFunc

    /**
     * This class should not be instantiated.
     * Please use use ready to use instance from {@link HrProxy.routes}.
     */
    constructor(private readonly serverUrl: URL) {
        this.notFoundHandlerFunc = ctx => ctx.respond({
            status: 404,
            body: `Handler not found for the "${ctx.request.method} ${ctx.request.path}" route on "${ctx.serverId}" server.`
        })
    }

    /**
     * Registers new route to request handler
     * @param method - HTTP method (`GET`, `POST`, `PUT`, ...)
     * @param path - URL path that should be bound to the handler e.g. https://demo.httprelay.io/proxy/myserver__/my/path__
     * @param name - unique route name that will be used to reference this route for generating URLs and else
     * @param handlerFunc - function that will be executed when client requests on the bound path
     */
    public add(method: string, path: string, name: string, handlerFunc: HandlerFunc): void {
        let route = new Route(method, path, name, this.serverUrl, handlerFunc)
        this.routes.push(route)
        this.routes.sort((a, b) => a.compare(b))
    }

    /**
     * Same as {@link add} but with the hardcoded `GET` method
     */
    public addGet(path: string, name: string, handlerFunc: HandlerFunc): void {
        this.add('GET', path, name, handlerFunc)
    }

    /**
     * Same as {@link add} but with the hardcoded `POST` method
     */
    public addPost(path: string, name: string, handlerFunc: HandlerFunc): void {
        this.add('POST', path, name, handlerFunc)
    }

    /**
     * Finds route by HTTP method and URL path
     * @param method - HTTP method (`GET`, `POST`, ...)
     * @param path - URL path
     *
     * Method is used internally for the routing.
     */
    public find(method: string, path: string): SelectedRoute {
        let routeParams: RouteParams = []
        let route = this.routes.find(r => {
            let matchRes = r.match(method, path)
            if (matchRes != null) {
                routeParams = matchRes
                return true
            }
            return false
        })

        return <SelectedRoute>{
            handlerFunc: route ? route.handlerFunc : this.notFoundHandlerFunc,
            params: routeParams
        }
    }

    /**
     * @returns - array of registered routes
     */
    public get all(): Route[] {
        return Array.from(this.routes)
    }

    /**
     * Get route by name
     * @param name - route unique name
     */
    public get(name: string): Route | undefined {
        return this.routes.find(r => r.name === name)
    }
}