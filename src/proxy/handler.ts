//*# nonModule !
import { HandlerFunc } from './route.js'
import HandlerCtx from './handler-ctx.js'
import { default as HandlerResponse, ResultBody } from './handler-response.js'

export default
//*/

/**
 * @internal
 */
class Handler {
    constructor(
        private readonly handlerFunc: HandlerFunc,
        private readonly wSecret?: string,
    ) {}

    public execute(ctx: HandlerCtx): Promise<RequestInit> {
        let rawResult = this.handlerFunc(ctx)
        return Promise.resolve(rawResult)
            .then(r => {
                let handlerResponse = (r instanceof HandlerResponse) ? r : new HandlerResponse(r) // If result is raw any object, making it HandlerFuncResponse
                handlerResponse.headers.set('HttpRelay-Proxy-JobId', ctx.jobId)
                return Promise.resolve(handlerResponse.body)
                    .then(b => Handler.requestInit(ctx.abortSig, this.wSecret, handlerResponse.headers, b))
            })
    }

    public static requestInit(abortSig: AbortSignal, secret: string='', headers: Headers = new Headers(), body?: ResultBody): RequestInit {
        if (secret) headers.set('HttpRelay-WSecret', secret)
        return <RequestInit> {
            method: 'SERVE',
            headers: headers,
            body: body,
            signal: abortSig
        }
    }
}