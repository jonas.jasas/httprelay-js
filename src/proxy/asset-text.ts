//*# nonModule !
import IAsset from './asset.js'
import {IAssetSettings} from './assets.js'

export default
//*/

/**
 * Class is responsible for storing text asset
 */
class AssetText implements IAsset {

    /**
     * Class should not be instantiated, please use {@link Assets.addText} instead.
     */
    constructor(
        private readonly str: string | (() => string),
        public readonly settings: IAssetSettings
    ) {}

    /**
     * Asset text. Same as {@link text} but without promise.
     * @returns - asset text
     */
    public get value(): string {
        return typeof this.str == 'string' ? this.str : this.str()
    }

    public blob(): Promise<Blob | undefined> {
        return Promise.resolve(new Blob([this.value], { type: 'text/html' }))
    }

    public text(): Promise<string | undefined> {
        return Promise.resolve(this.value)
    }
}