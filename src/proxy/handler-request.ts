//*# nonModule !
export default
//*/

class HandlerRequest {
    /**
     * @internal
     */
    constructor(private readonly response: Response) {
    }

    /**
     * Current request URL.
     */
    get url(): URL {
        return new URL(this.headerValue('HttpRelay-Proxy-Url'))
    }

    /**
     * Current request HTTP method e.g. `GET`, `POST`, `PUT`, etc.
     */
    get method(): string {
        return this.headerValue('HttpRelay-Proxy-Method')
    }

    /**
     * Current request URL path.
     */
    get path(): string {
        return this.headerValue('HttpRelay-Proxy-Path')
    }

    /**
     * Current request headers.
     */
    get headers(): Headers {
        return this.response.headers
    }

    /**
     * Current request body.
     */
    get body(): ReadableStream<Uint8Array> | null {
        return this.response.body
    }

    /**
     * Current request body.
     */
    public arrayBuffer(): Promise<ArrayBuffer> {
        return this.response.arrayBuffer()
    }

    /**
     * Current request body.
     */
    public blob(): Promise<Blob> {
        return this.response.blob()
    }

    /**
     * Current request body.
     */
    public formData(): Promise<FormData> {
        return this.response.formData()
    }

    /**
     * Current request body.
     */
    public json(): Promise<JSON> {
        return this.response.json()
    }

    /**
     * Current request body.
     */
    public text(): Promise<string> {
        return this.response.text()
    }

    /**
     * Returns current request header field value.
     *
     * @param name - header field name.
     */
    public headerValue(name: string): string {
        let value = this.response.headers.get(name)
        if (!value) throw new Error(`Unable to find "${name}" header field.`)
        return value
    }
}