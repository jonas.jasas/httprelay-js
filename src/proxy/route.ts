/**
 * HandlerFunc receives request context object `ctx` and can return any type of the value or `promise` that resolves to any type of the value.
 * Returned value will serialized and passed to the client.
 */
type HandlerFunc = (ctx: HandlerCtx) => any

//*# nonModule !
import HandlerCtx, {RouteParams} from './handler-ctx.js'
export { HandlerFunc }
export default
//*/

/**
 * Route class is responsible for binding routes with the handlers and building URLs.
 */
class Route {
    private readonly methodRx: RegExp
    private readonly pathRx: RegExp
    private readonly pathDepth: number

    /**
     * Route class constructor creates route instance.
     * Please use {@link Routes} class for the route management.
     */
    constructor(
        public readonly method: string,
        public readonly path: string,
        public readonly name: string,
        public readonly serverUrl: URL,
        public readonly handlerFunc: HandlerFunc,
    ) {
        this.methodRx = RegExp(method)
        this.pathRx = RegExp("^" + this.path.replace(/:[^\s/]+/g, '([\\w-]+)') + "$")
        this.pathDepth = this.path.split('/').length
    }

    /**
     * @internal
     */
    public compare(r: Route): number {
        let result = r.pathDepth - this.pathDepth
        if (result == 0) result = r.path.length - this.path.length
        return result
    }

    /**
     * @internal
     */
    public match(method: string, path: string): RouteParams | null {
        if (method.match(this.methodRx)) {
            let routeParams = path.match(this.pathRx)
            if (routeParams) return routeParams.slice(1)
        }
        return null
    }

    /**
     * Generates route URL and substitutes path parameters
     * @param params - route path parameters e.g. if the path is `/item/ITEMNO/color/ITEMCOLOR` then `{ITEMNO: 3, ITEMCOLOR: 'red'}`
     * @returns - URL that represents current route
     */
    public url(params: Object={}): URL {
        let path = this.path.substr(1)  // Removing / from the beginning
        Object.keys(params).forEach(p => {
            let rx = RegExp(`/${p}/g`)
            // @ts-ignore
            path = path.replace(rx, params[p])
        })
        let url = new URL(path, this.serverUrl)
        return url
    }
}