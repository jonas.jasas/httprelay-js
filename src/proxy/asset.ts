//*# nonModule !
import {IAssetSettings} from './assets.js'

export default
//*/

interface IAsset {
    readonly settings: IAssetSettings

    /**
     * Retrieve binary asset content.
     * @returns - promise that resolves to the blob containing asset
     */
    blob(): Promise<Blob | undefined>

    /**
     * Retrieve asset as text
     * @returns - promise that resolves to the asset text
     */
    text(): Promise<string | undefined>
}