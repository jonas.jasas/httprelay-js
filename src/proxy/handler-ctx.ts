type RouteParams = string[]

interface RespondParams {
    /**
     * Response body
     */
    body?: any

    /**
     * HTTP status code https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
     */
    status?: number

    /**
     * Response headers
     */
    headers?: Headers

    /**
     * Merge passed headers with the default ones
     */
    mergeHeaders?: boolean

    /**
     * File name if the response is a file. Adds `content-disposition` response header field.
     */
    fileName?: string

    /**
     * File should be downloaded or displayed in a browser. Controls `inline` / `attachment` values inside `content-disposition` response header field.
     */
    download?: boolean
}

//*# nonModule !
import HandlerRequest from './handler-request.js'
import HandlerResponse from './handler-response.js'
import Routes from './routes.js'
import Assets from './assets.js'

export { RouteParams }
export default
//*/

class HandlerCtx {
    /**
     *  Class should not be instantiated. Instance is passed to the handler function as a parameter value.
     */
    constructor(
        /**
         * Request information.
         */
        public readonly request: HandlerRequest,

        /**
         * Signals handler that result is no longer needed {@link HrProxy.stop} is executed.
         */
        public readonly abortSig: AbortSignal,

        /**
         * Current request route params e.g. `/proxy/serid/routeparam1/something/routeparam2`
         */
        public readonly routeParams: RouteParams,

        /**
         * Get all registered routes. Useful for generating URLs by route name.
         */
        public readonly routes: Routes,

        /**
         * Get all registered assets.
         */
        public readonly assets: Assets
    ) {}

    /**
     * Returns HTTP Relay server ID that has received request. E.g. `https://demo.httprelay.io/proxy/SERVERID/mypath`
     */
    get serverId(): string {
        return this.request.headerValue('HttpRelay-Proxy-ServerId')
    }

    /**
     * Returns current request job id. Each request has it's own unique random generated request ID.
     */
    get jobId(): string {
        return this.request.headerValue('HttpRelay-Proxy-JobId')
    }

    /**
     * Method should be used to return the result from the handler function with the additional response parameters.
     * @param result - response body and parameters.
     */
    public respond(result: RespondParams = {}): Promise<HandlerResponse> {
        return Promise.resolve(result.body)
            .then(b => new HandlerResponse(b, result.status, result.headers, result.mergeHeaders, result.fileName, result.download))
    }
}