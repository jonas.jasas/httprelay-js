//*# nonModule !
import IAsset from './asset.js'
import {IAssetSettings} from './assets.js'

export default
//*/

/**
 * Class is responsible for fetching assets from the remote HTTP servers as well as loading HTML embedded data URLs.
 */
class AssetFetch implements IAsset {
    private blobPro: Promise<Blob | undefined> | undefined

    /**
     * Class should not be instantiated, please use {@link Assets.addFetch} instead.
     */
    constructor(
        public readonly url: URL,
        public readonly settings: IAssetSettings
    ) {}

    public blob(): Promise<Blob | undefined> {
        if (!this.blobPro) {
            this.blobPro = fetch(this.url.href)
                .then(
                    r => (r.status >= 200 && r.status <= 299) ? r.blob() : undefined,
                    e => {
                            console.error(e)
                            return undefined
                    }
                )
        }
        return this.blobPro
    }

    public text(): Promise<string | undefined> {
        return this.blob().then(b => b ? b.text() : undefined)
    }
}