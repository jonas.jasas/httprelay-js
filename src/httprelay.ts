//*# nonModule !
import HrProxy from './proxy/proxy.js'

export default
//*/

/**
 * HttpRelay is a main class. It is responsible for instantiating `HTTP Relay` communication method objects.
 */
class HttpRelay {
    /**
     * HTTP Relay server URL
     */
    public readonly httpRelayUrl: URL

    /**
     * Url or your HTML document
     */
    public readonly baseUrl: URL

    /**
     * Creates HttpRelay object.
     * @param httpRelayUrl - URL that points to HTTP Relay server. Default: `https://demo.httprelay.io`
     * @param baseUrl - URL that will be used for generating route URLs. Default is taken from: document.baseURI
     */
    constructor(httpRelayUrl?: URL, baseUrl?: URL) {
        this.httpRelayUrl = httpRelayUrl ?? new URL('https://demo.httprelay.io')
        this.baseUrl = this.httpRelayUrl    // IGNORE! This is to calm down TS
        if (!baseUrl) {
            let baseUri = document?.baseURI
            if (!baseUri) throw new Error('Base URL "baseUrl" parameter is not set and unable to autodetect')
            this.baseUrl = new URL(baseUri)
        }
    }

    /**
     * Creates HTTP Relay `proxy` communication method object.
     * @param serverId - `proxy` communication method server ID e.g. https://demo.httprelay.io/proxy/`myServerId`. Default: random string
     * @param wSecret - write permission secret. If set it will lock `serverId` and only with this secret client requests cen be handled. Makes sure that unauthorized handlers are not serving clients.
     * @param path - custom path to proxy communication method endpoint. Default: `proxy/`
     * @param assetPathPrefix - asset path prefix.
     */
    public proxy(serverId?: string, wSecret?: string, path: string = 'proxy/', assetPathPrefix: string='') {
        serverId = serverId ?? btoa(Math.random().toString()).substr(5, 5)
        wSecret = wSecret ?? btoa(Math.random().toString()).substr(8, 8)
        return new HrProxy(new URL(path, this.httpRelayUrl), this.baseUrl, serverId, wSecret, assetPathPrefix)
    }
}