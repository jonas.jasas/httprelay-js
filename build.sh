#!/bin/bash

NON_MOD_DIR=./src-non-mod

trap cleanup EXIT
function cleanup {
  rm -rf $NON_MOD_DIR
}

rm -rf ./lib
cp -r ./src $NON_MOD_DIR
(
  cd $NON_MOD_DIR || exit 1
  find * -name "*.ts" | xargs -I {} ../node_modules/smart-preprocessor/bin/smart-preprocessor {} {} --nonModule
)

./node_modules/typescript/bin/tsc || exit 1
./node_modules/typescript/bin/tsc --project tsconfig-non-mod.json || exit 1